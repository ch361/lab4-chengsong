# Import SDK packages
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
import time
import json
import pandas as pd
import numpy as np

data_path = "vehicle0.csv"

certificate_formatter = "./certs/e22ba587f67a33f1eec295ff56737bbea67881186bacaf25daacbc36450ade8f-certificate.pem.crt"
key_formatter = "./certs/e22ba587f67a33f1eec295ff56737bbea67881186bacaf25daacbc36450ade8f-private.pem.key"
root_formatter = "./certs/root.ca.pem"

class MQTTClient:
    def __init__(self, device_id, cert, key):
        # For certificate based connection
        self.device_id = str(device_id)
        self.state = 0
        self.client = AWSIoTMQTTClient(self.device_id)
        #TODO 2: modify your broker address
        self.client.configureEndpoint("a277fdfp1hwp42-ats.iot.us-east-2.amazonaws.com", 8883)
        self.client.configureCredentials(root_formatter, key, cert)
        self.client.configureOfflinePublishQueueing(-1)  # Infinite offline Publish queueing
        self.client.configureDrainingFrequency(2)  # Draining: 2 Hz
        self.client.configureConnectDisconnectTimeout(10)  # 10 sec
        self.client.configureMQTTOperationTimeout(5)  # 5 sec
        self.client.onMessage = self.customOnMessage
        

    def customOnMessage(self,message):
        #TODO3: fill in the function to show your received message
        print("client {} received payload {} from topic {}".format(self.device_id, message.payload, message.topic))


    # Suback callback
    def customSubackCallback(self,mid, data):
        #You don't need to write anything here
        pass


    # Puback callback
    def customPubackCallback(self,mid):
        #You don't need to write anything here
        pass


    def publish(self, Payload="payload"):
        #TODO4: fill in this function for your publish
        self.client.subscribeAsync("max_emissin", 0, ackCallback=self.customSubackCallback)
        
        self.client.publishAsync("emission", Payload, 0, ackCallback=self.customPubackCallback)



print("Loading vehicle data...")
data = pd.read_csv(data_path)

print("Initializing MQTTClient...")
device_id = "car0"
client = MQTTClient(device_id, certificate_formatter, key_formatter)
client.client.connect()

idx = 0
while True:
    message = {}
    message['device_id'] = device_id
    message['emission'] = data["vehicle_CO2"][idx]
    messageJson = json.dumps(message)
    client.publish(messageJson)
    time.sleep(3)
    idx += 1




